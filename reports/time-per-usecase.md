## Use Case Time Management

| **UC**                 | **Documentation** | **Development** | **Testing** | **Warm-Up Time** | **Total** | **FP** |
|------------------------|-------------------|-----------------|-------------|------------------|-----------|--------|
| CRUD Group Management  |  5h 31m           | 11h 27m         |     00m     | 15m              | 17h 13m   |  25,35 |
| CRUD Task              | 17h 42m           | 33h 36m         |     00m     | 1h 15m           | 52h 33m   |  33,80 |
| CRUD User Management   |  3h 11m           | 17h 54m         |     00m     | 30m              | 21h 35m   |  20,80 |
| Display Exchange Board |  4h 15m           | 15h 43m         |     00m     | 30m              | 20h 28m   |  15,60 |
| Display Task List      |  8h 20m           | 10h 05m         |     00m     | 45m              | 19h 10m   |  22,75 |
| Login and Logout       |  5h               | 12h 38m         |     00m     |                  | 17h 38m   |  22,75 |
| Registration           |  5h 33m           |  6h 31m         |     00m     |                  | 12h 04m   |  16,25 |
| CRUD Contribution      |  1h 35m           |  0h 00m         |     00m     |                  | 01h 35m   |  15,90 |
| Configure Notification |  2h 05m           |  5h 15m         |     00m     |                  | 07h 20m   |  22,10 |

You can find the most recent [time report on our YouTrack](https://youtrack.unitasks.net/reports/time/138-3?line=issue&workType&remainingTime). 

The Function Points of CRUD Task, CRUD User Management, Display Task List, Register and Login Logout are based on the spent time of the first semester. Therefore, the function points don't fit anymore to the current total time.
The use of Hopper to display the notifications was decided later in the project. That's the reason why Hopper's function points have the given value. 
For the use cases CRUD Contribution and Display Exchange Board were 1273 minutes recorded. The implementation of the use case CRUD Contribution was combined with the implementation of the use case Display Exchange Board.

