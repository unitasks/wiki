# Use-Case Specification: Manage Notifications
#### unitasks


---
## Table of Contents

&emsp; [1. Manage Notifications](#1-manage-notifications)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>

## 1. Manage Notifications
### 1.1 Brief Description
tbd

## 2. Flow of Events
### 2.1 Basic Flow
tbd

#### 2.1.1 Activity Diagram
tbd

#### 2.1.2 Narrative
tbd

#### 2.1.3 Mockups
tbd

### 2.2 Alternative Flow
tbd

## 3. Preconditions
tbd

## 4. Postconditions
tbd