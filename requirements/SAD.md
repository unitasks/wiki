# unitasks<br/>Software Architecture Documentation
## Table of Contents

[1. Introduction](#1-introduction)<br/>
&emsp; [1.1 Purpose](#11-purpose)<br/>
&emsp; [1.2 Scope](#12-scope)<br/>
&emsp; [1.3 Definitions, Acronyms, and Abbreviations](#13-definitions-acronyms-and-abbreviations)<br/>
&emsp; [1.4 References](#14-references)<br/>
&emsp; [1.5 Overview](#15-overview) <br/>
[2. Architectural Representation](#2-architectural-representation)<br/>
[3. Architectural Goals and Constraints](#3-architectural-goals-and-constraints)<br/>
[4. Use-Case View](#4-use-case-view)<br/>
[5. Logical View](#5-logical-view)<br/>
&emsp; [5.1 Overview](#51-overview)<br/>
&emsp; [5.2 Architecturally Significant Design Packages](#52-architecturally-significant-design-packages)<br/>
[6. Process View](#6-process-view)<br/>
[7. Deployment View](#7-deployment-view)<br/>
[8. Implementation View](#8-implementation-view)<br/>
&emsp; [8.1 Overview](#81-overview)<br/>
&emsp; [8.2 Layers](#82-layers)<br/>
[9. Data View](#9-data-view)<br/>
[10. Size and Performance](#10-size-and-performance)<br/>
[11. Quality](#11-quality)<br/>
[12. Our Technologies](#12-technologies) <br/>
[13. Design Pattern](#13-design-pattern)

# Software Architecture Documentation

## 1. Introduction

### 1.1 Purpose
This document provides a comprehensive architectural overview of the system, by using several architectural views to depict different aspects of the system. It is intended to capture and convey the significant architectural decisions which have been made on the system.
### 1.2 Scope
This document describes the architecture of the unitasks application including all necessary infrastructure.
### 1.3 Definitions, Acronyms, and Abbreviations

| **Abbreviation** | |
| --- | --- |
| MVC | Model View Controller |
| N/A | Not applicable |
| UC | Use Case |

| **Definition** | |
| --- | -- |
| Software Architecture Document | A document that defines the architectural requirements and specifications of a certain software application |

### 1.4 References
| **Title** | **Date** | **Publishing Organization** |
| --- | --- | --- |
| [unitasks Navigation Page](https://unitasks.net/) | 2019-11-29 | unitasks Team |
| [unitasks Blog](https://blog.unitasks.net/) | 2019-11-29 | unitasks Team |
| [unitasks Staging](https://staging.unitasks.net/) | 2019-11-29 | unitasks Team |
| [unitasks Production](https://webapp.unitasks.net/) | 2019-11-29 | unitasks Team |
| [GitHub Source Code](https://git.unitasks.net) | 2019-11-29 | unitasks Team |
| [Project Management](https://youtrack.unitasks.net) | 2019-11-29 | unitasks Team |
| [Software Requirements Specification](/requirements/srs.md)  | 2019-11-29 | unitasks Team |

### 1.5 Overview
All necessary architectural details will be described in the following sections. This includes a database diagram, which gives an overview about the project structure.
Furthermore, the architectural representation, goals and constraints are clarified in this document.

## 2. Architectural Representation
Unitasks uses python 3 with the Flask framework for backend and the bootstrap library as well as React for frontend.

Flask itself doesn’t prescribe a strict MVC architecture, but because it demands so little structure it can be set up to act like one.
Furthermore, Flask mainly works with annotations to map URLs to functions, so there is no class structure to be seen. Instead we separate things into files and modules which can each be viewed as a kind of singleton. That still gives us a sort of object oriented syntax with what can be better described as a functional approach.

One could describe our PostgreSQL Database in combination with the Python logic as our model, the React Frontend as the view and the Flask API as our controller of a Model-View-Controller architecture.
Because of this structure we can’t generate a pretty class diagram. To make it easier to onboard new developers we have a detailed introduction to the project, which can be found [here](/docs/Onboarding.md).

Besides Flask there is also the communication with Postgres as our database, and a static frontend hosting container via nodeJS. While we did not explicitly optimize for a mobile device, with React as our implementation framework we could easily develop an android/ios app via React Native 

![Architecture](/img/architecture-diagram.png)

## 3. Architectural Goals and Constraints

As described, Flask uses a Model-View-Controller architecture. This should lead to a clear distinction between data, logic and views.
The Flask framework in combination of SQL alchemy does also improve code security. Due to the clear separation between user input and internal commands, the risk of attacks like SQL injections and cross-site scripting should be decreased to a minimum.

## 4. Use-Case View
Below, you can find the overall use case diagram that shows all use cases the application should provide.

![Overall Use Case Diagram](/requirements/img/OUCD.png)

Here you can find the various use case specification documents:
*  [CRUD Task](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Task.md)
*  [CRUD Contribution](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Contribution.md)
*  [CRUD User Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20User%20Management.md)
*  [CRUD Group Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Group%20Management.md)
*  [Display Exchange Board](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Exchange%20Board.md)
*  [Login and Logout](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Login%20and%20Logout.md)
*  [Display Task List](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Task%20List.md)
*  [Registration](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Register.md)


## 5. Logical View
### 5.1 Overview
The following section gives an overview about the logical structure of how the application is implemented. The strict rules regarding the MVC architecture should provide the necessary separation between functional logic and user interface design.
### 5.2 Architecturally Significant Design Packages
As described in [2. Architectural Representation](#2-architectural-representation) we use Flask, so we don't have a real class structure so to say. Nevertheless, we try to use singleton classes and OO design principles.
One could describe our Postgre SQL Database as our model, our HTML templates as our view and Flask as our controller.
## 6. Process View
N/A
## 7. Deployment View
![Deployment View](https://gitlab.com/unitasks/wiki/raw/master/img/Deployment%20view.png)

## 8. Implementation View
### 8.1 Overview
N/A
### 8.2 Layers
N/A
## 9. Data View
The auto-generated database diagram below shows the entities with all their attributes that are needed for the application to work. The diagram also provides an overview of the foreign key relationships between the tables.
![Database Diagram MVC](https://gitlab.com/unitasks/wiki/raw/master/img/DB-Schema.png)

The ER-diagram also shows the concept of the database.
![ER-Diagram](https://gitlab.com/unitasks/wiki/raw/master/img/ER-Diagram.svg)

## 10. Size and Performance
The performance of the application will be affected by the number of users accessing the application, as this will increase the number of tasks, groups, notifications, etc. As everything needs to be stored in the database, this might lead to longer response times and will have an impact on the user experience.

Nevertheless, the application should be able to handle the currently expected number of users. In case any performance issues are detected, the hosting plan can be upgraded at any time to provide enough (database) server capacity.

## 11. Quality
We are using Gitlab as a continuous integration tool to ensure a high quality of our development process. 
Whenever there is a new commit to any branch, Gherkin tests, Selenium tests and unit tests are executed automatically. Pull requests to the staging (master) branch must not be permitted until all required tests have passed successfully to ensure a working application at any time.

In this [document](https://gitlab.com/unitasks/wiki/blob/master/org/server-development.md) you can find the workflow conventions of unitasks, which provide a high quality.
##### Example workflow: 
![example worklfow](https://gitlab.com/unitasks/wiki/raw/master/org/GitlabFlow.svg)

Additionally, the application must be developed to provide the best possible security. Released code needs to be checked regularly regarding the following types of attack:

- SQL injections
- Cross-site scripting (XSS)
- Cross-site request forgery (CSRF)

Furthermore, the python source code of the application must follow the [PEP 8 Style Guide](https://www.python.org/dev/peps/pep-0008/) to guarantee readability and one style of code written by multiple authors.

## 12. Our Technologies

For Project Management we use a self-hosted instance of YouTrack, which guides us in our development efforts. 
The major attractor of them is Python with Flask as a web framework. To edit all of our files we use Pycharm as our IDE which integrates nicely with YouTrack for Issues and Time Tracking. 
Lastly we use React to render our website in the browser.

For our version control sytem we use Gitlab which also doubles as a CI/CD pipeline that builds docker containers and tracks metrics via codacy.
Our container orchestration is done through use docker-compose on a Hetzner VPS including a PostgreSQL instance.

![Technologies](/img/technology.png)

## 13. Composite Pattern

As part of our refactoring efforts, we have moved our frontend to React.

The intent of the Composite Design Pattern is according to the Gang Of Four “to compose objects into tree structures to represent whole-part hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.”

A use case of the Composite Design pattern is for example a file system. In a file system the files represent leaves and directories composites of the tree structure. Each composite can contain components (files/directories), each of which could be a composite.

This pattern also fits perfectly with frontend development as UI containers follow often the whole-part hierarchy. 
