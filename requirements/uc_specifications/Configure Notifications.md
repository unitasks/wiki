# Use-Case Specification: Configure Notifcations

---

## Table of Contents

&emsp;[1. Brief Description](#1-brief-description)  
&emsp; [2. Flow of Events](#2-flow-of-events)  
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)  
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)  
&emsp;&emsp;&emsp; [2.1.2 Mockups](#213-mockups)  
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)  
&emsp; [3. Preconditions](#3-preconditions)  
&emsp; [4. Postconditions](#4-postconditions)  
&emsp; [5. Function Points](#5-function-points)  

## 1. Brief Description

This use case allows users to configure their notifications. There are different types of notifications such as notifications for new activities in a exchange board of one of your tasks or new tasks in one of your groups.
A user can receive notifications via the third party app "[Hopper](https://account.hoppercloud.net/)".

The use of Hopper to display the notifications was only decided later in the project. Therefore the mockups are not up to date.

## 2. Flow of Events

### 2.1 Basic Flow

#### Create notification

1. The system creates notifications for each new contribution in an exchange board or for each new task/subtask of a group.

#### Receive notifications

1. A users needs to login to the Hopper application (https://account.hoppercloud.net/)
2. At the dashboard of the hopper application all notifications are displayed.

#### Select notification platform

1. A user can select the hopper notification platform by entering the user management page.
2. There the user needs to press the "Add Hopper notifications" button which redirects him to Hopper.
3. After creating a Hopper account or logging in to Hopper the user can accept the connection of the two applications.
4. Afterthat, he can receive notifications on Hopper.

#### Filter notifications

1. Within the Hopper webapp a user can filter notifications of a specific origin.

#### 2.1.1 Activity Diagram

![Activity Diagram Notifications](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_NOTIFICATIONS.png?inline=false)

#### 2.1.3 Mockups

![Landing Page with Notification icon](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Configure%20Notifications/Landing%20Page.png)

![Notification configuration menu](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Configure%20Notifications/Notification%20configuration%20menu.png)

### 2.2 Alternative Flow

tbd

## 3. Preconditions

The main preconditions for this use case are:

1. The user is signed in.
2. The user is on the landing page (task list).

## 4. Postconditions

The notification configuration could be updated.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      22,1 Function Points
    Time spent: 410 minutes
    Estimated time: 797 minutes

![Function Points Configure Notifications ](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/Notifications.png)
