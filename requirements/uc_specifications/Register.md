# Use-Case Specification: Register
#### unitasks

---
## Table of Contents

&emsp; [1. Register](#1-Register)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  


## 1. Register

### 1.1 Brief Description

This use case allows guest users to create themselfs a unitask account.
## 2. Flow of Events

### 2.1 Basic Flow
1. An unregistered person enters the unitasks landing page.
2. The person clicks on the Register / create Account button.
3. The person fills the presented form.
4. The system checks the form for validity.
5. The system send a confirmation mail.
6. The person follows the link in the mail.
7. The system creates a new user and logs it in.
8. The system redirects the user to their task list.

#### 2.1.1 Activity Diagram

![Activity Diagram](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UseCaseRegister.svg?inline=false)

### 2.1.2 Narrative

```gherkin
Feature: Register
    As a user
    I want to register myself
    so that I can login in to the system.

Scenario: valid form fill
    Given user is on register page
    And user filled register form
    When user presses confirm button
    And form data is valid
    Then user receives confirmation mail

Scenario: invalid form fill password
    Given user is on register page
    And user filled register form password incorrectly
    When user presses confirm button
    And form data password is invalid
    Then user is prompted to correct password

Scenario: invalid form fill field
    Given user is on register page
    And user filled register form field incorrectly
    When user presses confirm button
    And form data field is invalid
    Then user is prompted to correct field

Scenario: confirm Mail
    Given user has received confirmation mail
    When user clicks on confirmation link
    Then system creates a new account
    And user is redirected to logged in landing page
```
[Feature file](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Feature%20files/register.feature)

#### 2.1.3 Mockups

##### Empty registration screen 
![Use-Case Register](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/registration/Registration.png?inline=false)

##### Filled registration screen 
![Use-Case Register filled](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/registration/Registration%20Screen%20filled%20in.png?inline=false)

##### Successful registration popup 
![Use-Case Register filled](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/registration/Successful%20registration.png?inline=false)

##### Registartion failed due to missing fields screen
![Use-Case Register filled](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/registration/Missing%20fields.png?inline=false)

##### Registartion failed because passwords didn't match screen
![Use-Case Register filled](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/registration/Wrong%20password%20confirmation.png?inline=false)



## 3. Preconditions

The main preconditions for this use case are:

1. The user is not signed in
2. The user is on the landing page
3. The user doesn't have an account

## 4. Postconditions

The user is registered in the system and is now able to login with their email and password.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      16,25 Function Points
    Time spent: 408 minutes

![Function Points Register](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/Register.png)