# Use-Case Specification: CRUD User Management
#### unitasks


---
## Table of Contents

&emsp; [1. CRUD User Management](#1-crud-user-management)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. CRUD User Management

### 1.1 Brief Description
This use case allows users to administer their user. The User Management includes changing user specific data (like username, email, password, etc.) or deleting the whole user.

## 2. Flow of Events
### 2.1 Basic Flow
#### Display user
1. A user enters their User Management page by clicking on the user icon of the landing page of the webapp (see [Task List](requirements/uc_specifications/Display%20Task%20List.md))
2. On the page all user data is displayed. Namely the current user name, display-name and email of the user.
3. By clicking on the "Go Back"-button or "Task List" in the menu bar the user returns to the landing page.

#### Edit user 
1. A user enters the edit process of their data by clicking on the edit icon of the User Management page.
2. The user can change their name, display-name, email and password.
3. For changing the password the current password and a password confirmation of the new password are required.
4. By clicking on the "Save"-button the unitasks system checks the provided data and continues only on valid inputs. The user is asked to correct the invalid data otherwise or add some missing information.
4. After that, the changed user data is updated in the system and the user gets notified about the successful update of their user data.
5. By clicking the "Go Back" button or after the successful update of the user data, the user is redirected to their user management page.

#### Delete user
1. A user enters the delete process of their user by clicking on the delete icon of the User Management page.
2. To approve the deletion of the whole user a pop-up opens up.
3. By clicking the "Delete" button of the pop-up the user is deleted in the system, gets removed from all affected groups and the user is logged out.
4. By clicking the "Go Back" button of the pop-up the user is redirected to their User Management page, the user is not deleted.

#### 2.1.1 Activity Diagram
![Activity Diagram User-Management](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_USER_MANAGEMENT.png?inline=false)

#### 2.1.2 Narrative
#### Display User
```gherkin
Feature: Display User
    As a user
    I want to see my profile
    so that I can check my user data.

Scenario: display user data
  Given user is on landing page
  When user presses the user icon
  Then user is navigated to their user management page
   And the systems displays their user data

Scenario: go back button pressed
   Given user is on landing page
    And user presses the user icon
  When user presses go back button
  Then user is redirected to task list
```

#### Edit User
```gherkin
Feature: Edit User
    As a user
    I want to edit my profile
    so that all user data is up-to-date.

Scenario: valid form fill
  Given user is on user management page
    And user presses edit icon
    And user fills out form
  When user presses save button
    And form data is valid
  Then user data is updated in the system
    And user gets notified about successful update
    And user is redirected to user management page

Scenario: invalid form fill
   Given user is on user management page
    And user presses edit icon
    And user fills out form
  When user presses save button
    And form data is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct data

Scenario: valid password change
   Given user is on user management page
    And user presses edit icon
    And user wants to change password
  When user presses save button
    And input data is valid
  Then password is changed in the system

Scenario: invalid password change - wrong old password
   Given user is on user management page
    And user presses edit icon
    And user wants to change password
  When user presses save button
    And old password is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct password

Scenario: invalid password change - wrong confirmation
   Given user is on user management page
    And user presses edit icon
    And user wants to change password
  When user presses save button
    And password repetition is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct password

Scenario: cancel button pressed
   Given user is on user management page
    And user presses edit icon
  When user presses cancel button
  Then user is redirected to user management page
```
#### Delete User
```gherkin
Feature: Delete User
    As a user
    I want to delete a my user profile
    so that my data is deleted in the system.
    
Scenario: confirmed deletion
    Given user is on user management page
     And user presses delete icon
    When user approves the deletion of the user profile
    Then user is deleted in the system
     And user is logged out

Scenario: unconfirmed deletion
    Given user is on user management page
     And user presses delete icon
    When user doesn't approves the deletion of the user profile by clicking the cancel button
    Then the user is not deleted in the system
     And the user is redirected to their user management page
```
#### 2.1.3 Mockups
##### Display User
![Display user](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/User%20Management/Display%20user.png)

##### Edit User
![Edit user](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/User%20Management/Edit%20user.png)
###### Error pop up for wrong password confirmation
![Error](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/User%20Management/Error%20password%20confirmation.png)
##### Delete User
![Delete User](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/User%20Management/Delete%20user.png)

### 2.2 Alternative Flow
Planned features:
Link to...
* Overview of all tags of the user, CRUD Tag
* Overview of all groups, CRUD Group Management

## 3. Preconditions
The main preconditions for this use case are:
1. The user is signed in.
2. The user is on the landing page of the webapp.

## 4. Postconditions
### Display User
No postconditions.
### Edit User
The changed user data is saved in the database.
### Delete User
The user is deleted in the system and can not login anymore. 

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      20,8 Function Points
    Time spent: 629 minutes

![Function Points CRUD User Management](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/User.png)
