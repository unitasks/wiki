# Use-Case Specification: Display Task List
#### unitasks


---
## Table of Contents

&emsp; [1. Display Task List](#1-display-task-list)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. Display Task List
### 1.1 Brief Description
This use case allows users to display their Task List including all Personal and Group Tasks, which is also the landing page of the unitasks Webapp. 
There are different filters applicable and the user can change the sort of the Task List.
Therefore many features can be reached from the Task List, for example the detail page of a certain Task or the input screen to create new Tasks. 
Furthermore, in the Task List the status of Tasks can directly be changed.

## 2. Flow of Events
### 2.1 Basic Flow
##### Task List
1. A Student enters the unitasks webapp, after the log in he/she gets redirected to the Task List.
2. The unitasks system detects all pending tasks of the user and displays them in the Task List.
3. There are various possibilities for the user to interact with the Task List:
    1. By single clicking on a Task where applicable the Sub Tasks of the selected Task are shown in the Task List.
    2. By double clicking on a Task the user is navigated to the detail page of this Task.
    3. By clicking on the edit icon of a Task the user is navigated to the update page of this Task (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)). For updating a Task the respective permissions are required (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)).
    4. By clicking on the delete icon of a Task the user is asked to confirm the deletion of this Task (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)). For deleting a Task the respective permissions are required (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)).
    5. By filling in the checkbox of a Task the user can mark a Task as done.
    6. By clicking on the filter icon the user can adjust the filters of the Task List.
    7. By clicking on the sort icon the user can adjust the order of Tasks in the Task List.
    8. By clicking on the "Add Task" button the user is navigated to the add new Task page (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)).
    9. By filling in the Search field the user can search for existing Tasks.

##### Sort menu
1. The user clicks on the sort icon of the Task List.
2. In a Pop-Up the user can select the desired sorting criteria.
3. After that, the system automatically applies the selected order to the Task List.

##### Filter menu
1. The user clicks on the filter icon of the Task List.
2. In a Pop-Up the user can select the desired filters.
3. After that, the system automatically applies the selected filters to the Task List.

##### Detail page
1. The user double clicks a Task in the Task List and is navigated to the detail page of this Task.
2. There are various possibilities for the user to interact with the detail page:
    1. By clicking the "Go Back" button the user returns to the Task List.
    3. By clicking on the edit icon of a Task the user is navigated to the update page of this Task (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)). For updating a Task the respective permissions are required (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)).
    4. By clicking on the delete icon of a Task the user is asked to confirm the deletion of this Task (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)). For deleting a Task the respective permissions are required (see [CRUD Task](requirements/uc_specifications/CRUD%20Task.md)).
    5. By filling in the checkbox of a Task the user can mark a Task as done.

#### 2.1.1 Activity Diagram
![Activity Diagram](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_display_tasklist.svg)

#### 2.1.2 Narrative
##### Task List
```gherkin
Feature: Task List
    As a Student
    I want to see all pending Tasks
    so that I get informed about my Tasks and can work on them.
    
Scenario: Display of Task List
    Given the user logs in to the system
    When the user is navigated to their Task List
    Then the systems displays all pending Tasks.

Scenario: Display Sub Tasks
    Given the user is on Task List page
    When the user clicks on a Task
     And the Task has Sub Tasks.
    Then the SubTasks are shown below the Task in the Task List.

Scenario: Display detail page of Task
    Given the user is on Task List page
    When the user double clicks on a Task
    Then the user is navigated to the detail page of the Task.

Scenario: Display edit icon of a Task
    Given the user is on Task List page
    When the user has the required permission to update a selected Task
    Then the edit icon of the Task is shown in the Task List.

Scenario: Display update page of a Task
    Given the user is on Task List page
    When the user clicks on the edit icon of a Task
      And has the required permission to update a selected Task
    Then the user is navigated to the update page of the Task.

Scenario: Display delete icon of a Task
    Given the user is on Task List page
    When the user has the required permission to update a selected Task
    Then the delete icon of the Task is shown in the Task List.

Scenario: Display delete confirmation pop-up
    Given the user is on Task List page
    When the user clicks on the delete icon of a Task
      And has the required permission to update a selected Task
    Then a pop-up opens up to approve the deletion of the selected Task.

```
##### Filter menu
```gherkin
Feature: Filter menu
    As a Student
    I want to filter my Tasks
    so that only the relevant Tasks for me are displayed.

Scenario: Selection of filters
    Given the user is on the Task List page
    When selects a filter for the Task List.
    Then the Tasks are filtered by the selected filter.
```

##### Sort menu
```gherkin
Feature: Sort menu
    As a Student
    I want to change the order of my Tasks
    so that the most relevant Tasks for me are displayed at the top of the Task List.

Scenario: Selection of sort criteria
    Given the user is on the Task List page
    When selects a sort criteria for the Task List.
    Then the Tasks are sorted by the selected criteria.
```
##### Search field
```gherkin
Feature: Search for Tasks
    As a Student
    I want to search for a specific Task
    so that I can find the Task to display, edit or delete it.

Scenario: Search for a Task
    Given the user is on the Task List page
    When the user enters a search term
    Then the systems automatically searches for Tasks with titles or descriptions containing the entered term
      And the systems displays the found Task in the Task List.
```

#### 2.1.3 Mockups
##### Task List
![Task List](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Task%20List.png)
#### Task List with filter menu
![Filter menu](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Filter%20menu.png)
#### Task List with sort menu
![Sort menu](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Sort%20menu.png)
#### Detail page of a Task
![Detail page](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Detail%20page.png)

### 2.2 Alternative Flow
All possible flows are described at [2.1 Basic Flow](#21-basic-flow).

## 3. Preconditions
The user needs to be logged in.

## 4. Postconditions
* The adjustment of filters and sorting criteria are saved.
* The status of the Tasks is saved.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      22,75 Function Points
    Time spent: 986 minutes

![Function Points Display Task List](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/TaskList.png)
