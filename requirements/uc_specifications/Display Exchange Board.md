# Use-Case Specification: Display Exchange Board
#### unitasks


---
## Table of Contents

&emsp; [1. Display Exchange Board](#1-display-exchange-board)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. Display Exchange Board
### 1.1 Brief Description
This use case allows users to display the exchange board of a selected task.

## 2. Flow of Events
### 2.1 Basic Flow
1. The user double clicks a Task in the Task List and is navigated to the detail page of this Task.
2. Below some details of the task the exchange board of the selected task is shown.

#### 2.1.1 Activity Diagram

![Activity Diagram](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_display_exchange.svg)

#### 2.1.3 Mockups
##### Display Exchange Board
![Display Exchange Board](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Exchange%20Board/Display%20Exchange%20Board.png)

### 2.2 Alternative Flow
none

## 3. Preconditions
The main preconditions for this use case are:

1. The user is logged in.
2. There is at least one task in his/her task list.

## 4. Postconditions
none

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      15,6 Function Points
    Time spent: 611 minutes
    Estimated time: 516 minutes

For the use cases CRUD Contribution and Display Exchange Board were 1273 minutes recorded. The time spent value above is the share by function points of this use case.

![Function Points Display Exchange Board](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/ExchangeBoard.png)
