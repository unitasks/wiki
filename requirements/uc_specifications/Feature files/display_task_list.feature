Feature: Display tasklist
    Displays a list containing all pending tasks for the user


    Scenario: User views landing page
        Given the user is on the landing page
        And the user is logged in
        Then the user sees a list of all pending tasks
    
    Scenario: User vies tasklist
        Given the user is on the tasklist page
        Then the user sees a list of all pending tasks

    Scenario Outline: User filters tasklist
        Given the user is on the tasklist page
        When the user filters for <criteria>
        Then the user sees all tasks with criteria <criteria>

        Examples:
        | criteria    |
        | done        |
        | pending     |
        | in progress |
