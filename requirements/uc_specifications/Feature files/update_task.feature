Feature: Update Task
    As a Group Admin
    I want to update a task
    so that I can correct some properties.
    
Scenario: valid form fill
  Given user is on task list page
    And user presses edit task icon of one specific task
    And user fills out form
  When user presses update button
    And form data is valid
  Then the selected task is updated in the system
    And user gets notified about successful update
    And user is redirected to task list

Scenario: invalid form fill
   Given user is on task list page
    And user presses edit task icon of one specific task
    And user fills out form
  When user presses update button
    And form data is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct data or add missing data

Scenario: cancel button pressed
  Given user is on task list page
    And user presses edit task icon of one specific task
  When user presses cancel button
  Then user is redirected to task list
