Feature: Create Task
    As a Group Admin
    I want to create a new task
    so that the task list of each member of my course is up-to-date.

Scenario: valid form fill
  Given user is on task list page
    And user presses create task button
    And user fills out form
  When user presses submit button
    And form data is valid
  Then a new task is created in the system
    And user gets notified about successful creation
    And user is redirected to task list

Scenario: invalid form fill
   Given user is on task list page
    And user presses create task button
    And user fills out form
  When user presses submit button
    And form data is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct data

Scenario: cancel button pressed
   Given user is on task list page
    And user presses create task button
  When user presses cancel button
  Then user is redirected to task list
    
  
  