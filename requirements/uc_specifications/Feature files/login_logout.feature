Feature: Login and Logout
    As a registered user
    I want to login/logout myself
    so that I can sign in and sign out of the system.
    
Scenario: valid login
    Given user is on landing page
    And user filled login form
    When user presses login button
    And login data is valid
    Then user is logged in
    And is redirected to their task list.

Scenario: invalid login
    Given user is on landing page
    And user filled login form
    When user presses login button
    And login data is invalid
    Then user is prompted to correct their input.
    
Scenario: logout
    Given user is logged in
    When user presses logout button
    Then user is logged out.
