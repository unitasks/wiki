Feature: Delete Task
    As a Group Admin
    I want to delete a task
    so that the task list of each member of my course is up-to-date.
    
Scenario: confirmed deletion
    Given user is on task list page
     And user presses delete task icon of one specific task
    When user approves the deletion of the selected task
    Then the selected task is deleted in the system

Scenario: unconfirmed deletion
    Given user is on task list page
     And user presses delete task icon of one specific task
    When user doesn't approves the deletion of the selected task by clicking the cancel button
    Then the selected task is not deleted in the system
     And the user is redirected to their task list

Scenario: confirmed deletion from updating mode
    Given user is on update task page
      And user presses delete task icon
    When user approves the deletion of the selected task
    Then the selected task is deleted in the system

Scenario: unconfirmed deletion from updating mode
    Given user is on update task page
      And user presses delete task icon
    When user doesn't approves the deletion of the selected task by clicking the cancel button
    Then the selected task is not deleted in the system
     And the user is redirected to their task list