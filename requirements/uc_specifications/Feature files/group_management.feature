Feature: Group management

    Enables user to manage the members of a group

    Scenario Outline: make member group administrator
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        When <user_a> gives <user_b> administrator priviliges
        Then <user_b> is administrator

        Examples:
        | group | user_a | user_b |
        | test  | karl   | heinz  |
        | test2 | jens   | hans   |

    Scenario Outline: join group email
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        And user <user_b> is not member og the group <group>
        When <user_a> add the email of user <user_b> to the group
        Then <user_b> gets an invitation

        Examples:
        | group | user_a | user_b |
        | test  | karl   | kalle  |
        | test2 | jens   | dieter |

    Scenario Outline: join group link
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        And user <user_b> is not member og the group <group>
        When <user_a> creates an invitation link
        And <user_b> clicks on invitation link
        Then <user_b> is member of group <group>

        Examples:
        | group | user_a | user_b |
        | test  | karl   | kalle  |
        | test2 | jens   | dieter |

    Scenario Outline: join group link not a user
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        And user <user_b> is not registered to unitasks
        And user <user_b> is not member og the group <group>
        When <user_a> creates an invitation link
        And <user_b> clicks on invitation link
        Then <user_b> is prompted to register

        Examples:
        | group | user_a | user_b |
        | test  | karl   | kalle  |
        | test2 | jens   | dieter |

    Scenario Outline: demote administrator
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        And user <user_b> is member of group <group>
        And user <user_b> administrator of group <group>
        When <user_a> demotes <user_b>
        Then <user_b> is no longer administrator

        Examples:
        | group | user_a | user_b |
        | test  | karl   | heinz  |
        | test2 | jens   | hans   |
    
    Scenario Outline: demote self error
        Given a group <group> exists
        And user <user_a> is member of group <group>
        And user <user_a> administrator of group <group>
        And <user_a> is the only administrator of <group>
        When <user_a> demotes <user_a>
        Then <user_a> gets notified that he has to elect another administrator first
