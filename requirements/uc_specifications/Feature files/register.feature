Feature: Register

   This feature allows the user to register himself.

Scenario: valid form fill
    Given user is on register page
    And user filled register form
    When user presses confirm button
    And form data is valid
    Then user receives confirmation mail

Scenario: invalid form fill password
    Given user is on register page
    And user filled register form password incorrect
    When user presses confirm button
    And form data password is invalid
    Then user is prompted to correct password

Scenario: invalid form fill field
    Given user is on register page
    And user filled register form field incorrect
    When user presses confirm button
    And form data field is invalid
    Then user is prompted to correct field

Scenario: confirm Mail
    Given user has received confirmation mail
    When user clicks on confirmation link
    Then system creates a new account
    And user is redirected to logged in landing page
