# Use-Case Specification: CRUD Task
#### unitasks


---
## Table of Contents

&emsp; [1. CRUD Task](#1-crud-task)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. CRUD Task

### 1.1 Brief Description

This use case allows Group Admins to create, update and delete Tasks for one of their groups. 
Each Task requires a task name, description, due date and group. In addition Tasks can be linked to a notification.
Every student is able to display their Group Tasks and their Personal Tasks in their Task List (see Use Case: Display Task List).

The features Rapla link and tag system were removed from scope as they are not necessary anymore.


## 2. Flow of Events
### 2.1 Basic Flow

#### Create Task
1. A Group Admin enters the task creation process by clicking the "Create task" button on their task list.
2. The person fills in the required information like "task name", "due date" and "group" and transmits it via a click on the submit button (tag is optional).
3. The unitasks system checks the provided data and continues only on valid inputs. The user is asked to correct the invalid data otherwise or add some missing information.
4. After that, depending on whether the user selects a certain group or 'Personal Task', a Group Task or a Personal Task is created in the system and the user gets notified about the successful creation of their task.
5. By Clicking the "Cancel" button or after the successful creation of a task, the user is redirected to their task list.


#### Update Task
1. A Group Admin enters the task update process by clicking the "Edit task" icon of one specific task on their task list.
2. The person can update all fields (task name, description, due date, group and tag).
3. The unitasks system checks the provided data and continues only on valid inputs. The user is asked to correct the invalid data otherwise or add some missing information.
4. After that, the selected Task is updated in the system and the user gets notified about the successful update of their task.
5. By clicking the "Cancel" button or after the successful creation of a Task, the user is redirected to their task list.

#### Delete Task
1. A user enters the task delete process by clicking the "Delete task" icon of one specific task on their task list or in updating mode of one specific task. Group Tasks can only deleted by Group Admins, Personal Tasks by the owner.
2. To approve the deletion of the selected task a pop-up opens up.
3. By clicking the "Delete" button of the pop-up the selected task is deleted in the system and gets removed from all affected task lists.
4. By clicking the "Cancel" button of the pop-up the user is redirected to their task list, the task is not deleted.

#### 2.1.1 Activity Diagram
##### Create Task
![Activity Diagram Add-new-task](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UseCaseCreateTask.svg?inline=false)

##### Update Task
![Activity Diagram Update task](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_Update_task.svg?inline=false)

##### Delete Task
![Activity Diagram Delete task](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_Delete_task.svg?inline=false)


#### 2.1.2 Narrative
##### Create Task
```gherkin
Feature: Create Task
    As a Student
    I want to create a new Task
    so that the Task List of each member of my group or my personal Task List is up-to-date.

Scenario: valid form fill
  Given user is on task list page
    And user presses create task button
    And user fills out form
  When user presses submit button
    And form data is valid
  Then a new task is created in the system
    And user gets notified about successful creation
    And user is redirected to task list

Scenario: invalid form fill
   Given user is on task list page
    And user presses create task button
    And user fills out form
  When user presses submit button
    And form data is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct data

Scenario: cancel button pressed
   Given user is on task list page
    And user presses create task button
  When user presses cancel button
  Then user is redirected to task list
```
[Feature file](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Feature%20files/create_task.feature)

##### Update Task
```gherkin
Feature: Update Task
    As a Student
    I want to update a task
    so that I can correct some properties.
    
Scenario: valid form fill
  Given user is on task list page
    And user presses edit task icon of one specific task
    And user fills out form
  When user presses update button
    And form data is valid
  Then the selected task is updated in the system
    And user gets notified about successful update
    And user is redirected to task list

Scenario: invalid form fill
   Given user is on task list page
    And user presses edit task icon of one specific task
    And user fills out form
  When user presses update button
    And form data is invalid
  Then user gets notified about the problem
    And user is prompted to enter correct data or add missing data

Scenario: cancel button pressed
  Given user is on task list page
    And user presses edit task icon of one specific task
  When user presses cancel button
  Then user is redirected to task list
```
[Feature file](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Feature%20files/update_task.feature)

#### Delete Task
```gherkin
Feature: Delete Task
    As a Student
    I want to delete a task
    so that the Task List of each member of my course or my personal Task List is up-to-date.
    
Scenario: confirmed deletion
    Given user is on task list page
     And user presses delete task icon of one specific task
    When user approves the deletion of the selected task
    Then the selected task is deleted in the system

Scenario: unconfirmed deletion
    Given user is on task list page
     And user presses delete task icon of one specific task
    When user doesn't approves the deletion of the selected task by clicking the cancel button
    Then the selected task is not deleted in the system
     And the user is redirected to their task list

Scenario: confirmed deletion from updating mode
    Given user is on update task page
      And user presses delete task icon
    When user approves the deletion of the selected task
    Then the selected task is deleted in the system

Scenario: unconfirmed deletion from updating mode
    Given user is on update task page
      And user presses delete task icon
    When user doesn't approves the deletion of the selected task by clicking the cancel button
    Then the selected task is not deleted in the system
     And the user is redirected to their task list
    
```
[Feature file](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Feature%20files/delete_task.feature)

#### 2.1.3 Mockups
##### Create Task
###### Add new task screen
![Mockup 1](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/add%20task/add%20new%20task.png?inline=false)

###### Missing fields error
![Mockup 2](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/add%20task/Missing%20fields.png?)

###### Screen filled in
![Mockup 4](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/add%20task/filled%20in.png?inline=false)

##### Update Task
###### Screen filled in
![Update Task filled in screen](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Update%20Task/Update%20Task.png)

###### Missing fields error
![Update Task missing fields](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Update%20Task/Missing%20fields.png)

##### Delete Task
###### Delete icon in updating mode
![Delete icon](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Delete%20Task/Delete%20Task.png)

###### Confirm delete in updating mode
![Confirm delete](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Delete%20Task/Approval%20delete%20task.png)

###### Confirm delete in Task List
![Delete from Task List](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Delete%20Task%20from%20Task%20List%20-%20Approval.png)

##### Delete / update icon in Task List
![icons in task list](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Display%20Task%20List/Task%20List.png)

### 2.2 Alternative Flow
* A task can also be deleted by clicking the delete icon in the updating mode of a task (see mockup).
* It's also possible to create Sub Tasks within a Task.

## 3. Preconditions
The main preconditions for this use case are:

1. The user is signed in.
2. The user is a Group Admin, if he/she wants to manage a Group Task. For a Personal Task the user doesn't need a special role.
3. The user is on the landing page (task list).

## 4. Postconditions
### Create Task
After synchronizing with the sever, the task gets distributed to the assigned courses and appears in all the affected task lists.

### Update Task
The task has been updated in all task lists of the affected course.

### Delete Task
The task has been deleted in all task lists of the affected course.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      33,8 Function Points
    Time spent: 1510 minutes

![Function Points CRUD Task](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/Task.png)
