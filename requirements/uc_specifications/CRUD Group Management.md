# Use-Case Specification: CRUD Group Management
#### unitasks


---
## Table of Contents

&emsp; [1.CRUD Group Management](#1-crud-group-management)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  


## 1. CRUD Group Management
### 1.1 Brief Description
This Use Case contains the use cases CRUD Group, CRUD Group Member and CRUD Group Admin.  
CRUD Group allows every user to create, view, edit and delete groups. By creating a group the user becomes the first Group Admin of this group.  
CRUD Group Member allows Group Admins to add, display and remove group members of their group. Furthermore Group Members can leave a group.  
CRUD Group Admin allows Group Admins to add and remove Group Admins. 
All groups of a user are displayed in the group management page, all information including the group members of a specific group are displayed in the group member management page of a group.

## 2. Flow of Events
### 2.1 Basic Flow

#### 2.1.1 CRUD Group
##### Create Group (Everyone)
1. A user enters their Group Management page by clicking on the "My Group" button on their landing page of the webapp.
2. By clicking on the add icon on the Group Management the user is navigated to the group member management page of a empty group.
3. The user enters a group title and a description.  
3.1 By clicking on the "Save"-Button the user input is validated. In case of a succesfull validation, the group is saved.
3.2 By clicking on the "Create join Link"-Button the user can create a join link which makes it easy for other students to join the group. The Group Admins needs to copy and share this link.
3.3. By clicking on the "Add"-Icon the user can add group members manually (see Add Group Member).

##### Display Groups (Everyone)
1. A user enters their Group Management page by clicking on the "My Group" button on their landing page of the webapp.
2. On the page all groups of the user are displayed including the groups in which the user is a group admin as well as those groups in which the user is a normal member.
3. Clicking on the "Go Back"-button or "Task List" in the menu bar the user returns to the landing page.

##### Edit Group (Group Admin)
1. A user enters the edit group process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user can change the attributes of the group such as the group name and the group description by clicking on the "Edit"-button.
4. By clicking on the "Save"-button the unitasks system checks the provided data and continues only on valid inputs. The user is asked to correct the invalid data otherwise or add some missing information.
5. After that, the changed group data is updated in the system.

##### Delete Group (Group Admin)
1. A user enters the delete group process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user clicks on the delete icon of the whole group.
4. To approve the deletion of the whole group a pop-up opens up.
5. By clicking the "Delete" button of the pop-up the group is deleted in the system, gets removed from all affected users and all group tasks are deleted.


#### 2.1.2 CRUD Group Member

##### Add Group Members (Group Admin)
1. A user enters the delete group process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user clicks on the add icon of the group.
4. A pop-up opens up to enter a username which should be added to the group.
5. By clicking on the "Add"-Button of the Pop-Up the input is validated. If a user with the given username exists, the user is added to the group.

##### Display Group Members (Group Admin)
1. A user enters their Group Management page by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. On this page all group members are displayed.

##### Remove Group Members (Group Admin)
1. A user enters the remove member process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user clicks on the remove icon of one selected group member.
4. To approve the removal of the selected group member of the group a pop-up opens up.
5. By clicking the "Remove" button of the pop-up the group member is removed from the group.


#### 2.1.3 CRUD Group Admin 

##### Add Group Admin
1. A user enters the add group admin process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user checks the Group Admin checkbox of a group member.
4. The selected group member becomes a group admin.

##### Remove Group Admin
1. A user enters the remove group admin process by clicking on the "My Group" button on their landing page of the webapp.
2. By selecting a group in which the user is a group admin, the user is navigated to the group member management page of the selected group.
3. The user unchecks the Group Admin checkbox of a group member.
4. The selected group admin becomes a normal group member.


#### 2.1.1 Activity Diagram
![Create Group](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_create_group.png)

![Edit Group](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_edit_group.png)

![My Group](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_my_group.png)

![Group Admin](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_group_admin.png)

#### 2.1.2 Narrative
tbd

#### 2.1.3 Mockups
##### Group Management
![Group Management](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Group%20Management/Group%20Management.png)

##### Create Group
![Create Group](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Group%20Management/New%20Group.png)

##### Group Member Management

![Group Member Management](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Group%20Management/Group%20Member%20Management.png)

##### Delete Group

![delete Group](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Group%20Management/Delete%20Group.png)

##### Leave Group

![Leave Group](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Group%20Management/Leave%20Group.png)

### 2.2 Alternative Flow

#### Leave Group (Group Member)
1. A user enters their Group Management page by clicking on the "My Group" button on their landing page of the webapp.
2. On the page all groups of the user are displayed. 
3. The user clicks on the leave icon of a group.
4. To approve the leave of the selected group a pop-up opens up.
5. By clicking the "Leave" button of the pop-up the group member is removed from the group.

## 3. Preconditions
The main preconditions for this use case are:

1. The user is signed in.
2. The user is on the landing page (task list).

## 4. Postconditions
If some configuration changes, it will be updated in the system and distributed to all affected users.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      25,35 Function Points
    Time spent: 1000 minutes
    Estimated time: 968 minutes

![Function Points CRUD Group Management](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/GroupManagement.png)