# Use-Case Specification: Login and Logout
#### unitasks


---
## Table of Contents

&emsp; [1. Login and Logout](#1-login-and-logout)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.2 Narrative](#212-narrative)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. Login and Logout
### 1.1 Brief Description
This use case allows registered users to log in to the system with their email and password.

## 2. Flow of Events
### 2.1 Basic Flow
#### Login
1. A registered user enters the unitasks landing page.
2. The user clicks on the login button.
3. The system checks the login data.
4. If the user enters a existing combination of email and password, he/she is redirected to the their task list.
5. In case of an incorrect combination of email and password the user is prompted to correct the login data.
6. If the users presses 'Forgot password', an email is sent with a link to reset the passwort.

#### Logout
1. A logged in user clicks on the logout button.
2. The user is logged out and gets redirected to the unitask landing page.

#### 2.1.1 Activity Diagrams
![Login activity diagramm](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_Login.svg)

![Logout activity diagramm](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/UML_Logout.svg?inline=false)

#### 2.1.2 Narrative

```gherkin
Feature: Login and Logout
    As a registered user
    I want to login/logout myself
    so that I can sign in and sign out of the system.
    
Scenario: valid login
    Given user is on landing page
    And user filled login form
    When user presses login button
    And login data is valid
    Then user is logged in
    And is redirected to their task list.

Scenario: invalid login
    Given user is on landing page
    And user filled login form
    When user presses login button
    And login data is invalid
    Then user is prompted to correct their input.
    
Scenario: forgot password
    Given user is on landing page
    When user presses forgot password button
    Then an email is sent with a link to reset the password.
    
Scenario: logout
    Given user is logged in
    When user presses logout button
    Then user is logged out.
```
[Feature file](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Feature%20files/login_logout.feature)

#### 2.1.3 Mockups
#### Login Screen
![Mockup 1](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Login%20and%20Logout/Login%20page.png)

#### Screen filled in
![Mockup 2](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Login%20and%20Logout/Filled%20out.png)

#### Invalid login data
![Mockup 3](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/Login%20and%20Logout/Invalid%20login%20data.png)

### 2.2 Alternative Flow
None

## 3. Preconditions
The user is already registered in the system and has correct login data consisting of email and password

## 4. Postconditions
### Login 
In case of a correct login data the user will be logged in.
### Logout
The user will be logged out.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      22,75 Function Points
    Time spent: 975 minutes

![Function Points Login and Logout](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/LoginLogout.png)