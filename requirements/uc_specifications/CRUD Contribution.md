# Use-Case Specification: CRUD Contribution
#### unitasks


---
## Table of Contents

&emsp; [1. CRUD Contribution](#1-crud-contribution)<br/>
&emsp;&emsp; [1.1 Brief Description](#11-brief-description)<br/>
&emsp; [2. Flow of Events](#2-flow-of-events)<br/>
&emsp;&emsp; [2.1 Basic Flow](#21-basic-flow)<br/>
&emsp;&emsp;&emsp; [2.1.1 Activity Diagram](#211-activity-diagram)<br/>
&emsp;&emsp;&emsp; [2.1.3 Mockups](#213-mockups)<br/>
&emsp;&emsp; [2.2 Alternative Flow](#22-alternative-flow)<br/>
&emsp; [3. Preconditions](#3-preconditions)<br/>
&emsp; [4. Postconditions](#4-postconditions)<br/>
&emsp; [5. Function Points](#5-function-points)  

## 1. CRUD Contribution

### 1.1 Brief Description

This use case shows all existing messages and files of an exchange board of a specific task and allows users to add messages/files to the exchange board.

## 2. Flow of Events
### 2.1 Basic Flow

#### Write message
1. The user double clicks a Task in the Task List and is navigated to the detail page of this Task.
2. Below some details of the task the exchange board of the selected task is shown.
3. The user enters a message.
4. The users presses enter.
5. The message is added to the exchange board and visible for everyone.

#### Show messages
1. The user double clicks a Task in the Task List and is navigated to the detail page of this Task.
2. Below some details of the task the exchange board of the selected task is shown.
3. In the exchange board all existing messages are displayed.
4. Next to each message their sender and the status of the sender regarding the selected task (done/not done) is shown.

#### Upload file
1. The user double clicks a Task in the Task List and is navigated to the detail page of this Task.
2. Below some details of the task the exchange board of the selected task is shown.
3. The user enters a message and uploads a file (using the file upload button).
4. The users presses enter.
5. The message and file are added to the exchange board and visible for everyone.

#### 2.1.1 Activity Diagram
![Login activity diagramm](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_contrib_upload_delete.png)

![Login activity diagramm](https://gitlab.com/unitasks/wiki/raw/master/img/activity_diagram_view_contributions.png)

#### 2.1.3 Mockups
![Display Exchange Board](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/wireframes/CRUD%20Contribution/New%20Message.png)


### 2.2 Alternative Flow
none

## 3. Preconditions
The main preconditions for this use case are:

1. The user is logged in.
2. There is at least one task in his/her task list.


## 4. Postconditions
### Write message
A new message was added to the exchange board of a task.

### Upload file
A new file was uploaded to the exchange board of a task.

## 5. Function Points

To calulate the function points for a specific use case we used the [TINY TOOLS FP Calculator](http://groups.umd.umich.edu/cis/course.des/cis525/js/f00/harvey/FP_Calc.html).

    Score:      16,9 Function Points
    Time spent: 662 minutes
    Estimated time: 567 minut+es

For the use cases CRUD Contribution and Display Exchange Board were 1273 minutes recorded. The time spent value above is the share by function points of this use case.

![Function Points CRUD Contribution](https://gitlab.com/unitasks/wiki/raw/master/requirements/img/FP%20points/Contribution.png)