# Test plan

## 1.	Introduction
### 1.1	Purpose
The purpose of the Iteration Test Plan is to gather all of the information necessary to plan and control the test effort for a given iteration. 
It describes the approach to testing the software.
This Test Plan for **Unitasks** supports the following objectives:
-	Identifies the items that should be targeted by the tests.
-	Identifies the motivation for and ideas behind the test areas to be covered.
-	Outlines the testing approach that will be used.
-	Identifies the required resources and provides an estimate of the test efforts.

### 1.2	Scope
This document describes the used tests, as they are unit tests and functionality testing.

### 1.3	Intended Audience
This document is meant for internal use primarily.

### 1.4	Document Terminology and Acronyms
- **SRS**	Software Requirements Specification
- **n/a**	not applicable
- **tbd**	to be determined

### 1.5	 References
| Reference        | 
| ------------- |
| [SAD](SAD.md) | 
| [Function Points](docs/FP-Calculation.pdf) |

            
## 2.	Evaluation Mission and Test Motivation
### 2.1	Background
By testing source code, we ensure our application to run smoothly. The goal is to make sure, that our application does not run into any unexpected errors.
### 2.2	Evaluation Mission
The mission of this test plan is to prevent errors in production and ensure an outstanding software quality.
### 2.3	Test Motivators
Our testing is motivated by 
- quality risks 
- technical risks
- use cases 
- functional requirements

## 3.	Target Test Items
The listing below identifies those test items (software, hardware, and supporting product elements) that have been identified as targets for testing. 
This list represents what items will be tested. 

Items for Testing:
- Unitasks Backend (python / PSQL)
- Unitasks Frontend (JS(X) / HTML / CSS)

## 4.	Outline of Planned Tests
### 4.1	Outline of Test Inclusions
Unit Testing the backend, automatically security testing the produced docker containers as well as Integration testing (including API testing) for the backend. Additionally there already is reliabilty testing/monitoring on the staging and production pages.<br/>
End2End testing for the frontend.<br/>
### 4.2	Outline of Other Candidates for Potential Inclusion
Stress Testing the application and its servers.
### 4.3 Outline of Test Exclusions
Unit Testing in the frontend is currently not part of the planned strategy

## 5.	Test Approach
### 5.1 Initital Test-Idea Catalogs and Other Reference Sources
**n/a**
### 5.2	Testing Techniques and Types
#### 5.2.1 Unitasks Backend Testing (Unit Testing)
|| |
|---|---|
|Technique Objective  	| Several functions in the backend will be called, their results will be compared with predefined results |
|Technique 		| Data should be mocked. Backend will not be started, no datastore will be used |
|Oracles 		| Functions return the correct and expected data. |
|Required Tools 	| unittest (py module), Docker |
|Success Criteria	| expected responses, passing tests |
|Special Considerations	|     -          |


#### 5.2.1 Unitasks Backend Testing (API Testing Testing)
|| |
|---|---|
|Technique Objective  	| The backend should be started. Several request should be made to determine the correct functionality of the API. |
|Technique 		|  Should operate on mocked data. A new postgres database will be spun up for the duration of the test. |
|Oracles 		| Endpoints return the correct and expected data as well as the expected response codes. |
|Required Tools 	| Python, PostgresSQL, Docker, karate |
|Success Criteria	| expected responses, passing tests |
|Special Considerations	|     -          |

#### 5.2.2 Unitasks Frontend Testing
|| |
|---|---|
|Technique Objective  	| Every interaction with the frontend should function as intended. |
|Technique 		| Gherkhin tests that are implemented using selenium test the frontend functionality. Another database instance is spun up to avoid hurting production data.  |
|Oracles 		| backend returns expected data, information is shown as expected |
|Required Tools 	| Gherkin, Selenium, PostgresSQL |
|Success Criteria	| Expected behavior and passing tests |
|Special Considerations	|     -          |

#### 5.2.3 Business Cycle Testing
**n/a**

#### 5.2.4 User Interface Testing
|| |
|---|---|
|Technique Objective  	| The user interface should be user-friendly and intuitive designed. |
|Technique 		| [Usability tests](https://gitlab.com/unitasks/wiki/-/blob/master/docs/usability-test.md) and [survey](https://www.surveymonkey.de/r/CR9Z58Q) with possible endusers |
|Oracles 		| Opinion and impressions of endusers|
|Required Tools 	| Surveymonkey |
|Success Criteria	| Positive feedback from endusers, Criticism considered |
|Special Considerations	|     -          |

#### 5.2.5 Performance Profiling 
**n/a**

#### 5.2.6 Load Testing
**n/a**

#### 5.2.7 Stress Testing
**n/a**

#### 5.2.8	Volume Testing
**n/a**

#### 5.2.9	Security and Access Control Testing
**n/a**

#### 5.2.10	Failover and Recovery Testing
**n/a**

#### 5.2.11	Configuration Testing
**n/a**

#### 5.2.12	Installation Testing
|| |
|---|---|
|Technique Objective  	| Ensuring that the application can be automatically deployed to a web server |
|Technique 		| GitLab CI performs automatic deployment after all other tests passed. |
|Oracles 		| Any occurring errors during deployment are logged by GitLab CI. |
|Required Tools 	| GitLab CI (automatic test execution in VM), web hosting & service provider, e.g. Hetzner |
|Success Criteria	| 	Installation testing is successful if no errors occurred during GitLab CI deployment to Hetzner. |
|Special Considerations	|     -          |

## 6.	Entry and Exit Criteria
### 6.1	Test Plan
#### 6.1.1	Test Plan Entry Criteria
All tests run within docker containers. Every test-container has it's own unique environment that fits it's needs. The containers and their environements are part of the pipeline. They are summarized into the 'test' job.
#### 6.1.2	Test Plan Exit Criteria
When all tests have passed.
#### 6.1.3 Suspension and Resumption Criteria
n/a

## 7.	Deliverables
### 7.1	Test Evaluation Summaries
The output of the testing containers as well as the output of the IDE when running the tests locally.
### 7.2	Reporting on Test Coverage
n/a
### 7.3	Perceived Quality Reports
n/a
### 7.4	Incident Logs and Change Requests
n/a
### 7.5	Smoke Test Suite and Supporting Test Scripts
n/a
### 7.6	Additional Work Products
#### 7.6.1	Detailed Test Results
The detailed test results are available in the GitLab pipeline logs.

#### 7.6.2	Additional Automated Functional Test Scripts
n/a
#### 7.6.3	Test Guidelines
n/a
#### 7.6.4	Traceability Matrices
n/a

## 8.	Testing Workflow
Developers should execute tests locally before pushing source code. When merging into master, tests are executed automatically. We agreed to not push into master directly.
## 9.	Environmental Needs
This section presents the non-human resources required for the Test Plan.
### 9.1	Base System Hardware
The following table sets forth the system resources for the test effort presented in this Test Plan.

| Resource | Quantity | Name and Type |
|---|---|---|
| GitLab CI/CD server <br/> | 1 | Server that builds and deploys our code |
| URL | 1 | unitasks.net |
| app server | 1 | Server that hosts staging.unitasks.net and webapp.unitasks.net. It is important that this server has a docker deamon running |

### 9.2	Base Software Elements in the Test Environment
The following base software elements are required in the test environment for this Test Plan.

| Software Element Name | Version | Type and Other Notes |
|---|---|---|
| Linux | latest | Operating System |
| Docker | | Container Software |

### 9.3	Productivity and Support Tools
The following tools will be employed to support the test process for this Test Plan.

| Tool Category or Type | Tool Brand Name                              |
|-----------------------|----------------------------------------------|
| Code Hoster           | [gitlab.com](https://gitlab.com/unitasks/) |
| CI Service            | [GitLab CI / CD](https://gitlab.com/unitasks/server/pipelines) |

### 9.4	Test Environment Configurations
n/a

## 10.	Responsibilities, Staffing, and Training Needs
### 10.1	People and Roles
This table shows the staffing assumptions for the test effort.

Human Resources


| Role | Minimum Resources Recommended (number of full-time roles allocated) |	Specific Responsibilities or Comments |
|---|---|---|
| Test Manager | 1 | Keep an overview over test quality and coverage. <br> Provide time estimates for test creation. <br> Point out issues with tests. |
| Test Designer | 1 | Define type of tests to be written. <br> Define test architecture and integration. <br> Define test tools. |
| Tester | 1 |	Implements tests. <br> Executes tests. <br> Peer reviews tests. <br>|
| Implementer | 3 | Implements features. <br> Runs tests to see if feature implementation has issues.<br>  |

### 10.2	Staffing and Training Needs
**n/a**
## 11.	Iteration Milestones

| Milestone | Planned Start Date | Actual Start Date | Planned End Date | Actual End Date |
|---|---|---|---|---|
| Have Unit Tests | 11.05.2020 | 11.05.2020 | 04.06.2020 | **tbd**   |
| Tests integrated in CI | 18.11.2019 | as soon as using gherkin | 18.11.2019 | 18.11.2019 |


## 12.	Risks, Dependencies, Assumptions, and Constraints
| Risk | Mitigation Strategy	| Contingency (Risk is realized) |
|---|---|---|
| Unexpected failures in production | Cover all important functions in tests | Rollback deployment to last stable version |
## 13. Management Process and Procedures
**n/a**

## 14. Metrics

Metrics are types of measurements to quantify the quality of the source code of our software and the software itself.
We use two metrics in our project: Cyclomatic complexity and Code Style.

### 14.1 Cylomatic Complexity

Cyclomatic complexity is a software metric used to indicate the complexity of a program. It is a quantitative measure of the number of linearly independent paths through a program’s source code.

Since our backend is written in Python we’ll use [radon](https://pypi.org/project/radon/) to measure the metric. 

#### Before Refactoring

Radon: Raw Metrics  
![Radon Raw Metrics](img/radon_before.png)

Radon: Cyclomatic Complexity (with all A's hidden)  
![Radon Cyclomatic Complexity](img/radon_cyclo.png)

#### After Refactoring

Radon: After refactoring update_member  
![Radon Cyclomatic Complexity after](img/radon_after.png)

### 14.2 Code Style (PEP8)

This metric is a compound one, but it basically sums up all linting issues that violate the Pyhton style guide [PEP8](https://www.python.org/dev/peps/pep-0008/). To measure this we use [flask8](https://pypi.org/project/flake8/).

#### Before Refactoring

Pep8 Code Style Violations (116 in total)  
![Pep 8 violations](img/pep8_before.png)

#### After Refactoring

Pep8 Code Style Violations (15 in total)  
![Pep 8 violations after](img/pep_after.png)
