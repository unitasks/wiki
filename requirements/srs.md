# unitasks Software Requirements Specification

## Table of Contents

&emsp;[1. Introduction](#1-introduction)<br/>
&emsp;&emsp; [1.1 Purpose](#11-purpose)<br/>
&emsp;&emsp; [1.2 Scope](#12-scope)<br/>
&emsp;&emsp; [1.3 Definitions, Acronyms, and Abbreviations](#13-definitions-acronyms-and-abbreviations)<br/>
&emsp;&emsp; [1.4 References](#14-references)<br/>
&emsp;&emsp; [1.5 Overview](#15-overview)<br/>

&emsp;[2. Overall Description](#2-overall-description)<br/>
&emsp;&emsp;[2.1 Vision](#21-vision)<br/>
&emsp;&emsp;[2.2 Use Case Diagram](#22-use-case-diagram)<br/>
&emsp;&emsp;[2.3 Use Cases](#23-use-cases)<br/>

&emsp;[3. Specific Requirements](#3-specific-requirements)<br/>
&emsp;&emsp; [3.1 Functionality](#31-functionality)<br/>
&emsp;&emsp; [3.2 Usability](#32-usability)<br/>
&emsp;&emsp; [3.3 Reliability](#33-reliability)<br/>
&emsp;&emsp; [3.4 Performance](#34-performance)<br/>
&emsp;&emsp; [3.5 Supportability](#35-supportability)<br/>
&emsp;&emsp; [3.6 Design Constraints](#36-design-constraints)<br/>
&emsp;&emsp; [3.7 On-line User Documentation and Help System Requirements](#37-on-line-user-documentation-and-help-system-requirements)<br/>
&emsp;&emsp; [3.8 Purchased Components](#38-purchased-components)<br/>
&emsp;&emsp; [3.9 Interfaces](#39-interfaces)<br/>
&emsp;&emsp; [3.10 Licensing Requirements](#310-licensing-requirements)<br/>
&emsp;&emsp; [3.11 Legal, Copyright, and Other Notices](#311-legal-copyright-and-other-notices)<br/>
&emsp;&emsp; [3.12 Applicable Standards](#312-applicable-standards)<br/>

&emsp;[4. Supporting Information](#4-supporting-information)<br/>
&emsp;&emsp; [4.1 Helpful Links](#41-helpful-links)<br/>
&emsp;&emsp; [4.2 Contact](#42-contact)<br/>

## 1. Introduction

### 1.1 Purpose

This Software Requirements Specification is a description of the *unitasks web application* - hereinafter referred to as "the application", "the program", "the software", "the platform" or "it" - that is being developed by the *unitasks Team*.

This document clearly states all functional and non-functional requirements that need to be fulfilled by the program. Furthermore, the SRS gives an overview of the usability, reliability, application design, and the defined standards and use cases. This specification shall minimize the risk of failure and eliminate misunderstandings between the customer and the developers of this software.

### 1.2 Scope

The SRS applies to the entire *unitasks* project. The *unitasks* platform will be a place in which you can track your study related task’s progress, upload solutions and exchange with other students.

#### 1.2.1 Actors

There are three types of actors: **Guest**, **Student** and **Group Admin**.

- Guest: A person who visits the *unitasks* application without being registered.
- Student: A person who uses the application and is registered & logged in. A Student can attend groups.
- Group Admin: A Student who manages a group and group related tasks.

#### 1.2.2 Subsystems

| **Subsystem** | **Description** |
| --- | --- |
| Task| Central object of application, can be created, edited, deleted and viewed. |
| Exchange Board | A place where users can discuss a task. |
| User Management | Enables registration, login and logout.|
| Group Management | Site where the user group of each group can be defined and edited by the group admins. |
| Calendar | Possibility to select due date from calendar. |
| Task List | List of all pending tasks of a student. |
| Notifications | Possibility to receive notifications. | 

### 1.3 Definitions, Acronyms, and Abbreviations

| **Abbreviation** | |
| --- | --- |
| FAQ | Frequently Asked Questions |
| MVC | Model-View-Controller |
| N/A | Not applicable |
| SRS | Software Requirements Specification |
| tbd | to be determined |
| UC | Use Case |

| **Definition** | |
| --- | -- |
| SRS | A  description of a software system that has to be developed |

### 1.4 References

| **Title** | **Date** | **Publishing Organization** |
| --- | --- | --- |
| [unitasks Navigation Page](https://unitasks.net/) | 2019-11-18 | unitasks Team |
| [unitasks Blog](https://blog.unitasks.net/) | 2019-11-18 | unitasks Team |
| [unitasks Staging](https://staging.unitasks.net/) | 2019-11-18 | unitasks Team |
| [unitasks Production](https://webapp.unitasks.net/) | 2019-11-18 | unitasks Team |
| [GitHub Source Code](https://git.unitasks.net) | 2019-10-14 | unitasks Team |
| [Project Management](https://youtrack.unitasks.net) | 2019-10-14 | unitasks Team |
| [Software Architecture Documentation](/requirements/SAD.md)  | 2019-11-29 | unitasks Team |

### 1.5 Overview

The chapters of this Software Requirements Specification are about the following aspects:

- Chapter 2 will give a general overview about the project. This includes the vision and the overall use case diagram.

- Chapter 3 contains the requirements, i.e. the functionality, usability, reliability and performance of the application. This also includes the provided interfaces, licensing and applicable standards.

- Any supporting information can be found in chapter 4.

## 2. Overall Description

### 2.1 Vision
Every student knows the problem of keeping track of their study-related tasks. What needs to be done by when? Where can I find the documents for my tasks? Who can I ask in case of questions or the need of help?
Especially in higher semesters, in which each student has a personal selection of lectures, or at universities with large courses the last question quickly becomes a problem. Which  students attend the same lecture as me? Who maybe has the same problems with the lecture related tasks? And who has already worked out a solution?

We asked ourselves these questions too, which was the moment, when the idea of unitasks was born.

Unitasks will be a platform for students on which each student receives their set of tasks based on their chosen lectures. 
The tasks can be added by the Group Admin of the respective course and can also be assigned to due dates as well as to notifications. 
Besides the task list, unitasks will also offer an exchange board for each task, where students attending the respective lecture could easily ask a question, discuss with other students about the task or upload files.

Unitasks will be a tool, which improves the daily workflow and daily life of many students. 


### 2.2 Use Case Diagram

![Overall Use Case Diagram](/requirements/img/OUCD.png)

### 2.3 Use Cases

*  [CRUD Task](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Task.md)
*  [CRUD Contribution](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Contribution.md)
*  [CRUD User Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20User%20Management.md)
*  [CRUD Group Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Group%20Management.md)
*  [Configure Notifcations](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Configure%20Notifications.md)
*  [Display Exchange Board](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Exchange%20Board.md)
*  [Login and Logout](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Login%20and%20Logout.md)
*  [Display Task List](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Task%20List.md)
*  [Registration](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Register.md)

## 3. Specific Requirements

### 3.1 Functionality

The following section points out all requirements and explains their functionality. Each of the subsections below represents a subsystem of the application.

#### 3.1.1 Task

Tasks are the central object of unitasks. 
There are three different types of tasks. Firstly, Personal Tasks that everyone can create and are only visible for the creator.
Secondly, there are Group Tasks. These tasks can only be created by Group Admins of a group and are assigned to this group (see 3.1.4 Group Management). 
Thirdly, there are Sub Tasks. Each task can contain Sub Tasks of the same type.

Besides that, each task can also be linked to a due date (see 3.1.5 Calendar), creates notifications automatically (see 3.1.7 Notifications) and has a status (done or not done).

All Students are able to display their Task List containing their Personal and their Group Tasks. 

#### 3.1.2 Exchange Board

For each task (see 3.1.1 Task) automatically exists a so-called Exchange Board, where every student of the respective user group of the Task can contribute. That means students can comment and discuss the Task, ask questions and upload files.

#### 3.1.3 User Management

When someone visits unitasks for the first time, the person is asked to create a new user with a user name, diplayname, email, and password.
After that, the user can log in with their credentials and can also edit or delete their user.
Any logged-in person possesses the role Student (or Group Admin), any logged out person is a Guest.

#### 3.1.4 Group Management

Every Student can create a group and becomes thereby the first Group Admin of this group.

In the Group Management Group Admins can configure their groups. That contains various features.
On the one hand Group Admins are able to see all group members and to add or delete course members manually. 
On the other hand they can create a join link and share it (e.g. via an existing email distribution list) to give other students easily the opportunity to join the group.
Moreover Group Admins can determine the Group Admins of a group, as long as there is always at least one Group Admin.

In addition Tasks (see 3.1.1 Task) can be assigned to a group or be removed by Group Admins. After a Task is assigned to a group every group member receives this Task in their Task List (see 3.1.6 Task List).
Groups are not to be equated with lectures, they just describe a user group that attends one or more of the same lectures.

#### 3.1.5 Calendar

With a date picker, you can assign a due date to each Task.

#### 3.1.6 Task List

In the Task List, every student is able to see their overview of Tasks including Personal and Group Tasks. Besides that, it is possible to mark Tasks as done, access a detail page of each Task and the exchange board or set Task filters.

### 3.1.7 Notifications

In the notification menu, each user can personalize their notification preferences. You can select whether and on which platform you want to receive notifications as well as set task- or group-specific notification filters.

### 3.2 Usability

The goal is to create a web application that is user-optimized and easy to handle. We want the user to feel comfortable to use all of the tools we provide.

#### 3.2.1 Self-Explanatory

It is important to us, to make the understanding of the application as easy as possible, so that no additional explanation is needed. The user shall be able to use the application intuitively.

#### 3.2.2 Device independent

The user should be able to access the application with any internet capable device. Therefore, the application must be developed for the following types of devices:

- desktop computers and laptops
- smartphones
- tablet devices

This requires a responsive design of the web application and tests on each of these device categories.

### 3.3 Reliability

#### 3.3.1 Availability of the application

The web application is currently hosted on Hetzner. As technical issues can never be eliminated, our application should ensure a 95% up-time.

#### 3.3.2 Defect rate

Each application release must pass all tests successfully in order to be deployed on the web server. Any bugs that are discovered in the production system need to be fixed within the next sprint.

> The term *bug* describes all behavior of the application that does not comply with this specification or is obviously erroneous. This does not include missing features. Bugs can be reported on [GitLab](https://git.unitasks.net).

Loss of user data is not acceptable under any conditions. All data must be backed-up regularly.

### 3.4 Performance

#### 3.4.1 Time to response

The developers need to ensure that the application responds within as little time as possible. The average response time should be below 2-3 seconds.

#### 3.4.2 Capacity

The application should be able to manage at least one thousand registered users with an average of 50 tasks per user. The response time should not exceed the specified limits with one hundred users being online at the same moment.

### 3.5 Supportability

#### 3.5.1 Coding Standards

The python source code of the application must follow the [PEP 8 Style Guide](https://www.python.org/dev/peps/pep-0008/) to guarantee readability and one style of code written by multiple authors.

#### 3.5.2 Maintainability

The deployment of this application must contain automated testing by a continuous integration service, so that any failure can be identified immediately.

### 3.6 Design Constraints

#### 3.6.1 Programming Languages

This application uses python 3 for backend and the bootstrap library as well as React, HTML, JavaScript and Less for frontend.
A Postgre SQL Database will be used for storing and managing data.

#### 3.6.2 MVC Architecture

The application must be designed using the model-view-controller architecture. Therefore, we decided to use Flask, a python framework with MVC support.

### 3.7 On-line User Documentation and Help System Requirements

We try to make our application as intuitive and easy-to-use as possible. Nevertheless, we will provide a FAQ page in our application and will discuss questions regarding the application in our [blog](https://blog.unitasks.net).
For questions during the development process we already offer a [FAQ page](http://blog.unitasks.net/faq/) on our blog.

### 3.8 Purchased Components

N/A

### 3.9 Interfaces

#### 3.9.1 User Interfaces

(tbd)

#### 3.9.2 Hardware Interfaces

N/A

#### 3.9.3 Software Interfaces

The application should be accessible by all major browsers, such as:

- Google Chrome
- Microsoft Edge
- Mozilla Firefox
- Opera

#### 3.9.4 Communications Interfaces

The web server is available over HTTPS on port 443. Any unencrypted connections over HTTP on port 80 are not supported and will be redirected to HTTPS.

### 3.10 Licensing Requirements

The *unitasks* application and all associated work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

### 3.11 Legal, Copyright, and Other Notices

This document makes use of the *generic he* for reasons of readability. Any terms containing the words "he", "himself", "his", etc. are meant to include both women and men, unless explicitly stated otherwise.

The *unitasks Team* will not take any responsibility for missed appointments or forgotten items caused by the use of this application, although we will try our very best to avoid such inconvenience.

### 3.12 Applicable Standards

Additionally, the application must be developed to provide the best possible security. Released code needs to be checked regarding the following types of attack:

- SQL injections
- Cross-site scripting (XSS)
- Cross-site request forgery (CSRF)

Manual security tests needs to be performed regularly.

## 4. Supporting Information

### 4.1 Helpful Links
- [Navigation Page](unitasks.net) - Overview of all links
- [unitasks Blog](https://blog.unitasks.net) - News regarding the project
- [GitHub Code](https://git.unitasks.net) - The up-to-date source code of our project
- [Project Managment](https://youtrack.unitasks.net) - The up-to-date project status
- [Staging Site](https://staging.unitasks.net) - The staging environment of our web application
- [Production Site](https://webapp.unitasks.net) - The live version of our web application

### 4.2 Contact

Feel free to comment on our blog or send an e-mail to: info@unitasks.net
Or contact us personally: 
* Philipp: philipp@unitasks.net
* Jonas: jonas@unitasks.net
* Hendrik: hendrik@unitasks.net