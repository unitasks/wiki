# Onboarding unitasks

- [Onboarding unitasks](#onboarding-unitasks)
  - [General Links](#general-links)
  - [Development Virtual Machine](#development-virtual-machine)
  - [Database Schema](#database-schema)
  - [Routes and Folder Structure](#routes-and-folder-structure)
    - [Folder Structure](#folder-structure)
    - [Routes](#routes)

## General Links

- [OUCD](../requirements/usecases.md): Get an overview of unitasks use cases
- [SRS](../requirements/srs): Get an overview of unitasks requirements
- [SAD](../requirements/SAD.md): Get an overview of unitasks architecture

## Development Virtual Machine

We recommend a linux, i.e. Ubuntu 18 LTS, virtual machine as a development environment for
unitasks.

In order to standardize the development environment, we created a separate project that
automatically provisions the VM via vagrant. You can follow its setup guide [here](https://gitlab.com/unitasks/development-machine).
    
## Database Schema

It is essential to understand our data model, since it is at the core of our application.

You can view the entity relationship diagram below:

![Entity Relationship Model](../img/DB-Schema.png)

## Routes and Folder Structure

Since we are using flask as our server framework, we do not have a way to create a pretty
diagram as an overview. Instead, you can find an explanation of the folder structure
and API routes below.

### Folder Structure

- `frontend`: Contains our frontend code (React, Docker container)
  - `unitasks-frontend/src`: Root of our react project
- `test`: Backend/frontend tests for our application
- `volumes`: The database and profiling volume for our docker-compose setup
- `webserver`: Our backend code
  - `api`: All of the `/api` routes in the flask project
  - `database`: The database configuation and schema
  - `static`: Directory for static file serving

### Routes

*Note: You can inspect all routes via `app.url_map` in `main.py`*

