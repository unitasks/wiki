# Usability Test Unitasks

## Scenario

You study at a university and want to use the unitasks webapp (staging.unitasks.net) to manage your study related tasks.

## Tasks

### Task 1

1. Create a new user and login.
2. Create a personal task with the following content:
   - Name: Finish Programming Homework
   - Description: See Worksheet 7, Task 1-3
   - Due date: Tomorrow
   - Create a subtask for each task of the worksheet (1-3)
3. Change the task name to "Important! Finish Programming Homework"

### Task 2

1. Create a new group with the following content:
    - Name: Awesome learning group
    - Description: Learning group for Programming
2. Add the member "MyBestFriend" to your group
3. Join another group of your lecture by the following join link: https://staging.unitasks.net/group/join/9uS8x4NsWNH0i77QDS9UFotCoHFOiH8V
4. Leave the Awesome learning group

### Task 3

1. Select a task and go to its exchange board
2. Ask a question: "Do we have to do task 4 as well?"
3. Upload a solution as a new root thread: "Here is my solution!".
4. Reply to your message with a emoji.

### Task 4

1. Confirm your email adresss.
2. Change your password.
3. Add hopper notifications.
4. Logout
