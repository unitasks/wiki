# Server Development

The following document outlines how we are going to develop the server
application for our unitasks Software Engineering project.

It is partly based on Gitlab Flow.

## Gitlab Flow

These are the general rules of gitlab flow, as outlined on the  [gitlab blog](https://about.gitlab.com/blog/2016/07/27/the-11-rules-of-gitlab-flow/):

1. Use feature branches, no direct commits on master.
2. Test all commits, not only ones on master.
3. Run all the tests on all commits
4. Perform code reviews before merges into master, not afterwards.
5. Deployments are automatic, based on branches or tags.
6. Tags are set by the user, not by CI.
7. Releases are based on tags.
8. Pushed commits are never rebased.
9. Everyone starts from master, and targets master.
10. Fix bugs in master first and release branches second.
11. Commit messages reflect intent.

## Branches

### Master Branch

The **master** branch contains the currently deployed staging version of our code.
Nobody is able to push here directly, but rather has to move through a feature branch.

As soon as a merge request to **master** is opened, it is to be reviewed by at least
one team member. If the change is approved, the issue is then merged and
automatically deployed to **staging.unitasks.net**.

### Production Branch

The development branch contains the currently deployed production version of our code.
Nobody is able to push here directly, but rather has to move through the **master** branch.

As soon as a merge request to **production** is opened, it is to be reviewed by at least
one team member. If the change is approved, the issue is then merged and
automatically deployed to **webapp.unitasks.net**.

### Feature Branch

The project can contain multiple feature branches, that should be named in relation
to the actual feature being developed. Developers are able to push directly to
these branches and are encouraged to frequently do so.

If a feature is ready for deployment, a merge request to the **master** branch is
opened. Please start your feature branches with the name **feature-**, bugfixing
branches start with **hotfix-**.

## Example Workflow

![GitlabFlow Example](GitlabFlow.svg)

- Create issue in [Youtrack](youtrack.unitasks.net)
- Open a new feature branch in the [server gitlab repository](https://gitlab.com/unitasks/server)
- Develop the feature in as many commits as needed until ready to deploy (& test locally)
- Open merge request to **master** branch
- Get one team member to review the changes
- If approved, merge into **master**, else go back to developing on feature branch
- *The change is now automatically deployed to [staging.unitasks.net](http://dev.unitasks.net)*
- Open merge request to **production** branch
- Get two team members to review and test the live version
- If approved, merge into **production**, else go back to developing on feature branch
- *The change is now automatically deployed to [webapp.unitasks.net](http://webapp.unitasks.net)*