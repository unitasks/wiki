# Organisation

This directory and all of its subdirectories contains the workflows, standard practices,
style guides etc. that we use to govern ourselves.

It should be kept up to date, new proposals are submitted by opening a new branch and
subsequent merge request.

## Table Of Contents

- [Unitasks Server Development](server-development.md)