# Unitasks Wiki

Welcome to our wiki!

## Table of Contents

- [Software Requirements Specification](requirements/srs.md)
- [Software Architecture Documentation](requirements/SAD.md)
- [Testplan](requirements/testplan.md)
- [Overall Use Case Diagram](requirements/usecases.md)
- [ER-Diagram](https://gitlab.com/unitasks/wiki/blob/master/img/ER-Diagram.svg)

### Use Case Specification Documents

*  [CRUD Task](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Task.md)
*  [CRUD Contribution](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Contribution.md)
*  [CRUD User Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20User%20Management.md)
*  [CRUD Group Management](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/CRUD%20Group%20Management.md)
*  [Configure Notifcations](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Configure%20Notifications.md)
*  [Display Exchange Board](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Exchange%20Board.md)
*  [Login and Logout](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Login%20and%20Logout.md)
*  [Display Task List](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Display%20Task%20List.md)
*  [Registration](https://gitlab.com/unitasks/wiki/blob/master/requirements/uc_specifications/Register.md)

### Organisation

- [Organisation: Readme](org/README.md)
- [Organisation: Unitasks Server Development](org/server-development.md)
